# seewitheyesclosed.com

See With Eyes Closed was a blog by Benjamin Hollon that received posts from January 1, 2020 through April 26, 2022. In January of 2023, Benjamin decided to close the blog. Some of the better posts were migrated to the new general-purpose blog, [Musings](https://benjaminhollon.com/musings/), which had been around since January 2022 but was billed as a secondary blog.

[All blogs by Benjamin Hollon](https://benjaminhollon.com/blogs/)

[Benjamin Hollon's personal site](https://benjaminhollon.com)

[Writing by Benjamin Hollon](https://benjaminhollon.com/writing/)
